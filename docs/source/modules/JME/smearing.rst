Smearing
==========

.. currentmodule:: Corrections.JME.smearing

.. contents::

``jetSmearerRDF``
-----------------

.. autoclass:: jetSmearerRDF
   :members:

``jetVarRDF``
-------------

.. autoclass:: jetVarRDF
   :members:

``metSmearerRDF``
-----------------

.. autoclass:: metSmearerRDF
   :members:

