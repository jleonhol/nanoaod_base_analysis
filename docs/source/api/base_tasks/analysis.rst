cmt.base_tasks.analysis
============================

.. automodule:: cmt.base_tasks.analysis

.. contents::

Class ``CreateDatacards``
-------------------------

.. autoclass:: CreateDatacards
   :members:
